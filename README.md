# An Instruction Based Roofline Method for AMD GPUs

When generating a roofline for AMD GPUs (e.g. the Radeon Instinct MI60 accelerator), the Empirical Roofline Toolkit (ERT) methodology \[1,2,3\] needs to be adapted to an instruction rate based roofline, as the AMD profiling tools do not support counting floating-point operations (FLOPs).

## Determine Parameters for Peak Rates using ERT

Run ERT as you nominally would to determine the peak bandwidth and FLOP rate. For this example, the test platform is an AMD MI60 GPU and ERT uses the HIP version of the driver and kernel. The configuration file `config.amd-instr-example.01` is included in the repository for this tutorial and can be used as a template to be modified for your environment.

```
$ cd Empirical_Roofline_Tool-1.1.0/AMD_GPU_Method
$ ../ert config.amd-instr-example.01

<lots of ERT output>

+-------------------------------------------------
| Empirical roofline graph:    'Results.amd-instr-example.01/Run.001/roofline.ps'
| Empirical roofline database: 'Results.amd-instr-example.01/Run.001/roofline.json'
+-------------------------------------------------
```

The result is contained in the file `Results.amd-instr-example.01/Run.001/roofline.json`.  In this example, the JSON file contains the following results.

```
      "gbytes": {
         "data": [
            <clip>
            [
               "DRAM",
               730.0
            ]
         ],
         "metadata": {
            <clip>
            "GPU_THREADS": 128,
            "FLOPS": 2,
            "GPU_BLOCKS": 1024,
            <clip>
         }
      },
      "gflops": {
         "data": [
            [
               "FP64 GFLOPs",
               7134.66
            ]
         ],
         "metadata": {
            <clip>
            "GPU_THREADS": 64,
            "FLOPS": 1024,
            "GPU_BLOCKS": 2048,
            <clip>
         }

```

For this analysis we are interested in the what parameters resulted in the peak bandwidth, and the peak FLOPs rate. The peak DRAM bandwidth was achieved using 2 FLOPS per kernel iteration, 1024 GPU\_BLOCKS per work item and 128 GPU\_THREADS per block. The peak FLOPs rate was achieved using 1024 FLOPS per kernel iteration, 2048 GPU\_BLOCKS per work item with 64 GPU\_THREADS per block.

The next step is to create a new configuration file using only the parameters that produce peak results. In this case, only specifying the FLOPS, GPU\_BLOCKS and GPU\_THREADS values determined above. In addition, the number of experiments is reduced to 1 and the minimum working set size is set equal to the maximum working set size. These modifications ensure only a single experiment is run per invocation.

The ERT configuration file specifies the maximum working set size, ERT_MEMORY_MAX, in bytes and the minimum working set size, ERT_WORKING_SET_MIN, in words. Hence, set `ERT_WORKING_SET_MIN=(ERT_MEMORY_MAX/8)`, where the word size in this example is 8 bytes. A simple `diff` showing the changes for the new configuration file are shown below.

```
$ diff config.amd-instr-example.01 config.amd-instr-example.02
4c4
< ERT_RESULTS Results.amd-instr-example.01
---
> ERT_RESULTS Results.amd-instr-example.02
23c23
< ERT_FLOPS     1,2,512,1024
---
> ERT_FLOPS     2,1024
28,29c28,29
< ERT_GPU_BLOCKS     128,256,512,1024,2048
< ERT_GPU_THREADS    1024,512,256,128,64
---
> ERT_GPU_BLOCKS     1024,2048
> ERT_GPU_THREADS    128,64
31c31
< ERT_NUM_EXPERIMENTS 3
---
> ERT_NUM_EXPERIMENTS 1
37c37
< ERT_WORKING_SET_MIN 128
---
> ERT_WORKING_SET_MIN 134217728
```

It is also necessary to modify the ERT driver `driver1.cxx`. Nominally ERT runs a sweep of trials working its way up to a maximum number. This maximum number of trials is usually what gives the best result for a given parameter set. For profiling, we only want a single set of trials and hence a single set of counts with a known parameter set. To do this modify `Driver/driver1.cxx` to change the sweep to a single iteration. In addition, note that in the configuration file the variable ERT_TRIALS_MIN is set to 10, which sets the number of trials used after the source modification. This should be adequate, but you can experiment with higher values to see if you get better performance.

```
$ git diff Drivers/driver1.cxx
diff --git a/Empirical_Roofline_Tool-1.1.0/Drivers/driver1.cxx b/Empirical_Roofline_Tool-1.1.0/Drivers/driver1.cxx
index f23bdfd..aee152e 100644
--- a/Empirical_Roofline_Tool-1.1.0/Drivers/driver1.cxx
+++ b/Empirical_Roofline_Tool-1.1.0/Drivers/driver1.cxx
@@ -385,7 +385,7 @@ void run(uint64_t PSIZE, T *buf, int rank, int nprocs, int *nthreads_ptr)
       });
 #endif

-      for (t = 1; t <= ntrials; t = t * 2) { // working set - ntrials
+      t = ntrials; { // only use maximum trial count
 #ifdef ERT_MPI
 #ifdef ERT_OPENMP
 #pragma omp master
```

It is not necessary to run all phases of ERT with this configuration, as we only want the binary for the next step.

```
$ ../ert --build-only config.amd-instr-example.02
```

## Profile ERT using AMD's rocProf profiling tool and measure instruction count

RocProf \[4\], and assumedly the AMD MI60 GPU, does not provide for measuring the FLOP count for a given kernel. However, you can measure the number of vector ALU instructions executed. This may, or may not, be directly related to the actual FLOP count, but it is a direct measure of computation and if we measure both the roofline and the application using the same method then we can infer an application's performance relative to a measured computational bound.

The rocProf instruction counter of interest is VALUInsts, and the global DRAM (HBM) data movement counters are FetchSize (KiBytes read) and WriteSize (KiBytes written). The full rocProf input file is given below.

```
$ cat prof-ai.txt
# Example rocProf input for determining instruction based arithmetic intensity
# Perf counters group 1, total KiBytes read
pmc : FetchSize
# Perf counters group 2, total KiBytes written
pmc : WriteSize
# Perf counters group 3, vector ALU instructions executed
pmc : Wavefronts VALUInsts
# Filter by dispatches range, GPU index and kernel names
# supported range formats: "3:9", "3:", "3"
range: 0 : 10
#gpu: 0 1 2 3
gpu: 0
kernel: block_stride
```

To get the profile, simply run rocProf with the above input file and the binaries generated above. Once using the binary with the parameters that gave the best bandwidth, and a second time with the binary and parameters that gave the best FLOPs rate using `config.amd-instr-example.01`

```
$ rocprof -i prof-ai.txt -o bandwidth.csv ./Results.amd-instr-example.02/Run.001/FLOPS.002/driver1.kernel1 1024 128
<lots of output>
$ rocprof -i prof-ai.txt -o instruction.csv ./Results.amd-instr-example.02/Run.001/FLOPS.1024/driver1.kernel1 2048 64
<lots of output>
$ cat bandwidth.csv
Index,KernelName,gpu-id,queue-id,queue-index,pid,tid,grd,wgr,lds,scr,vgpr,sgpr,fbar,sig,FetchSize,WriteSize,Wavefronts,VALUInsts
1,"void block_stride<double, 0>(unsigned int, unsigned int, double*) [clone .kd]",0,0,2,920215,920218,131072,128,0,0,8,24,0,0x0,10486090,10486284,2048,92226
$ cat instruction.csv
Index,KernelName,gpu-id,queue-id,queue-index,pid,tid,grd,wgr,lds,scr,vgpr,sgpr,fbar,sig,FetchSize,WriteSize,Wavefronts,VALUInsts
1,"void block_stride<double, 0>(unsigned int, unsigned int, double*) [clone .kd]",0,0,2,920575,920578,131072,64,0,0,12,24,0,0x0,10487244,10486373,2048,5324866
```

For the run to calculate bandwidth: FetchSize=10486090 and WriteSize=10486284. In the profile to calculate instruction rate: VALUInsts=5324866. The units for FetchSize and WriteSize are in KiB, and the units for VALUInsts is the ***average (i.e. not an exact count)*** instructions per work item. The number of work items is defined in the configuration file, ERT\_BLOCKS\_THREADS, which for this example is 131072.

The final step is to collect timing information. This is done outside of rocProf to avoid the overhead associated with profiling. Here we can simply run the ERT binary with the same parameters.

```
$ ./Results.amd-instr-example.02/Run.001/FLOPS.002/driver1.kernel1 1024 128
fp64
  1073741824           10       30063.892  21474836480   2684354560
<clip>
$ ./Results.amd-instr-example.02/Run.001/FLOPS.1024/driver1.kernel1 2048 64
fp64
  1073741824           10      195429.581  21474836480 1374389534720
<clip>
```

ERT reports: working set size (in bytes), the number of trials, the time in microseconds, the total number of bytes, and the total number of FLOPs. As a sanity check, we check these against values with those determined with `config.amd-instr-example.01` and they are within the margin of experimental error for ERT.

The maximum sustained bandwidth: <br>
= (21474836480 bytes) / (30063.892e-6 seconds) / (1e9 bytes per GB) <br>
= 714.3 GB/s.

The maximum sustained FLOP rate: <br>
= (1374389534720 FLOPs) / (195429.581e-6 seconds) / (1e9 FLOPs per GF) <br>
= 7032.7 GF/s.

Using the timing information from above, we now have all the data we need to calculate the respective rooflines:

Roofline GB/s = (FetchSize+WriteSize)\*1024 / time / 1e9 <br>
              = (10486090+10486284)\*1024 /  30063.892e-6 / 1e9 <br>
              = 714.3 GB/s.

Roofline GInsts/sec = (ERT\_BLOCKS\_THREADS\* VALUInsts) / time / 1e9 <br>
                    = (131072\*5324866) / 195429.581e-6 / 1e9 <br>
                    = 3571.3 GInsts/sec.

The bandwidth roofline derived from the profiling results matches that calculated by ERT. Comparing the instruction rate, each work item on average is seeing 1.96 FLOPs per instruction.

## Profiling an Application Kernel

This same method can be used to determine determine the arithmetic intensity and instruction rate of a given application's kernels.

The `prof-ai.sh` script used above should be used as a starting point, but it will need to be modified to suit your applications characteristics. In particular, you'll need to change the name of the `kernel` and define the `range` of kernel invocations to be profiled.

### Example AI calculation
TBD

## References

1. Roofline Performance Model, <https://crd.lbl.gov/departments/computer-science/par/research/roofline/>, accessed 10/27/2020
2. Empirical Roofline Toolkit, <https://crd.lbl.gov/departments/computer-science/par/research/roofline/software/ert/>, accessed 10/27/2020
3. CS Roofline Toolkit Repository, <https://bitbucket.org/berkeleylab/cs-roofline-toolkit/src/master/>, accessed 10/27/2020
4. AMD ROCm Profiler, <https://rocmdocs.amd.com/en/latest/ROCm_Tools/ROCm-Tools.html>, accessed 10/27/2020
