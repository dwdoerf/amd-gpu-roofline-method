#!/bin/bash

# Build biniaries for profiling
../ert --build-only config.amd-instr-example.02

# Profile
# Use parameters which give the peak bandwidth
ifile="prof-ai.txt"

# Bandwidth, set variables based on nominal ERT run
bw_exe="./Results.amd-instr-example.02/Run.001/FLOPS.002/driver1.kernel1"
bw_ofile="bandwidth.csv"
bw_blocks=1024
bw_threads=128
srun -n 1 rocprof -i $ifile -o $bw_ofile $bw_exe $bw_blocks $bw_threads

# Instruction Rate, set variables based on nominal ERT run
instr_exe="./Results.amd-instr-example.02/Run.001/FLOPS.1024/driver1.kernel1"
instr_ofile="instruction.csv"
instr_blocks=2048
instr_threads=64
srun -n 1 rocprof -i $ifile -o $instr_ofile $instr_exe $instr_blocks $instr_threads

# Results
echo -e "\n--------------------------------------------------------------------------------"
echo "Bandwidth results:"
srun -n 1 $bw_exe $bw_blocks $bw_threads >> $bw_ofile
cat $bw_ofile

echo -e "\nInstruction rate results:"
srun -n 1 $instr_exe $instr_blocks $instr_threads >> $instr_ofile
cat $instr_ofile

